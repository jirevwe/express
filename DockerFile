FROM node:6
WORKDIR /app
COPY package.json /app
COPY .env-docker /app/.env
RUN npm install
COPY . /app
EXPOSE 8080
CMD [ "npm", "test" ]