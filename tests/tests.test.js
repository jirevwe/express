const sumjs = require('../api/sum');
const request = require('supertest');
const app = require('../app');

const mockCallback = jest.fn();
sumjs.forEach([0, 1], mockCallback);

test('Mock Length', () => {
  expect(mockCallback.mock.calls.length).toBe(2);
});

test('mock length', () => {
  expect(mockCallback.mock.calls[0][0]).toBe(0);
});

// describe('POST /api/theme/all [page: 1, count: 1]', () => {
//   test('get all themes [page: 1, count: 1]', (done) => {
//     request(app)
//       .post('/api/theme/all')
//       .set({
//         'Content-Type': 'application/json',
//         token: '$2a$05$PbOSlGYnJBq47/Hun2WYmOZM4UCM0JWS.ajzkdST.vfxTfAyxYxTm',
//       })
//       .send({
//         page: 1,
//         count: 1,
//       })
//       .then((res) => {
//         expect(res.status).toBe(200);
//         done();
//       });
//   });
// });

// describe('POST /api/theme/all [page: -1, count: -1]', () => {
//   test('get all themes [page: -1, count: -1]', (done) => {
//     request(app)
//       .post('/api/theme/all')
//       .set({
//         'Content-Type': 'application/json',
//       })
//       .send({
//         page: -1,
//         count: -1,
//       })
//       .then((res) => {
//         expect(res.status).toBe(200);
//         done();
//       });
//   });
// });

// describe('POST /api/theme/all No params', () => {
//   test('get all themes', (done) => {
//     request(app)
//       .post('/api/theme/all')
//       .then((res) => {
//         expect(res.status).toBe(404);
//         done();
//       });
//   });
// });
