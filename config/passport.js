let passport = require('passport');
let randomstring = require("randomstring");
let LocalStrategy = require('passport-local').Strategy;
let Promise = require('bluebird');
let request = require('request');
let utils = require("../utils/utils");
let User = require("../models/user");

function ssha512(cleartext) {
    let passwordhasher = require('password-hasher');
    let hash = passwordhasher.createHash('ssha512', cleartext, new Buffer('83d88386463f0625', 'hex'));
    return passwordhasher.formatRFC2307(hash);
}

function setLong(cleartext) {
    let Crypto = require('crypto-js');
    return Crypto.AES.encrypt(cleartext, "$2a$05$d92IUG5ZHIpU0f8fvQitvOut05tuZdD4rDp5RF8BC/7zdFvUqBk52");
}

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

passport.use('admin.signup', new LocalStrategy({ usernameField: 'username', passwordField: 'password', passReqToCallback: true }, function (req, username, password, done) {
    req.checkBody('username', 'Username cannot be empty').notEmpty();
    req.checkBody('username', 'Username should be at least 5 characters').isLength({ min: 5 });
    req.checkBody('password', 'Invalid password').notEmpty();
    req.checkBody('password', 'Password should be at least 6 characters').isLength({ min: 6 });

    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            return result.array().map((elem) => elem.msg);
        }
    }).then(errors => {
        if (errors) {
            return done(errors.join(", "), null);
        }
        else {
            User.findOne({ username: username }, function (err, user) {
                if (err) {
                    return done(err);
                } else {
                    if (user && user.username == req.body.username) {
                        return done("User already exists");
                    } else {
                        let token = randomstring.generate({
                            length: 5,
                            charset: 'numeric'
                        });

                        let newUser = new User;
                        newUser.username = username;
                        newUser.password = newUser.encrypt(password);
                        newUser.security_token = newUser.encryptToken(token);

                        newUser.save()
                            .then(user => {
                                return done(null, newUser);
                            }).catch(err => {
                                return done(err);
                            });
                    }
                }
            });
        }
    });
}));

passport.use('admin.signin', new LocalStrategy({ usernameField: 'username', passwordField: 'password', passReqToCallback: true }, function (req, username, password, done) {
    req.checkBody('username', 'Username cannot be empty').notEmpty();
    req.checkBody('username', 'Username should be at least 5 characters').isLength({ min: 5 });
    req.checkBody('password', 'Invalid password').notEmpty();
    req.checkBody('password', 'Password should be at least 6 characters').isLength({ min: 6 });

    req.getValidationResult().then(function (result) {
        if (!result.isEmpty()) {
            return result.array().map((elem) => elem.msg);
        }
    }).then(errors => {
        if (errors)
            return done(errors.join(", "), null);
        else {
            User.findOne({ username: username })
                .then(user => {
                    if (user && user.username == req.body.username) {
                        if (user.validatePassword(req.body.password))
                            return done(null, user);
                        else
                            return done("Incorrect password");
                    } else {
                        return done("User doesn't exist");
                    }
                }).catch(err => {
                    return done(err);
                });
        }
    });
}));

// passport.use('customer.signup', new LocalStrategy({ usernameField: 'username', passwordField: 'password', passReqToCallback: true }, function (req, username, password, done) {
//     req.checkBody('username', 'Email cannot be empty').notEmpty();
//     req.checkBody('username', "Email isn't properly formatted").isEmail();
//     req.checkBody('password', 'Invalid password').notEmpty();
//     req.checkBody('password', 'Password should be at least 6 characters').isLength({ min: 6 });

//     req.getValidationResult().then(function (result) {
//         if (!result.isEmpty()) {
//             return result.array().map((elem) => elem.msg);
//         }
//     }).then(errors => {
//         if (errors)
//             return done(errors.join(", "), null);
//         else {
//             Customer.findOne({ username: username }, function (err, customer) {
//                 if (err) {
//                     return done(err);
//                 } else {
//                     if (customer && customer.username == req.body.username) {
//                         return done("Customer already exists");
//                     } else {
//                         let token = randomstring.generate({
//                             length: 5,
//                             charset: 'numeric'
//                         });

//                         let newCustomer = new Customer;
//                         newCustomer.username = username;
//                         newCustomer.password = newCustomer.encrypt(password);
//                         newCustomer.security_token = newCustomer.encryptToken(token);

//                         newCustomer.save()
//                             .then(user => {
//                                 return done(null, newCustomer);
//                             }).catch(err => {
//                                 return done(err);
//                             });
//                     }
//                 }
//             });
//         }
//     });
// }));

// passport.use('customer.signin', new LocalStrategy({ usernameField: 'username', passwordField: 'password', passReqToCallback: true }, function (req, username, password, done) {
//     req.checkBody('username', 'Email cannot be empty').notEmpty();
//     req.checkBody('username', "Email isn't properly formatted").isEmail();
//     req.checkBody('password', 'Invalid password').notEmpty();
//     req.checkBody('password', 'Password should be at least 6 characters').isLength({ min: 6 });

//     req.getValidationResult()
//         .then(function (result) {
//             if (!result.isEmpty()) {
//                 return result.array().map((elem) => elem.msg);
//             }
//         }).then(errors => {
//             if (errors)
//                 return done(errors.join(", "), null);
//             else {
//                 Customer.findOne({ username: username })
//                     .then(customer => {
//                         if (customer && customer.username == req.body.username) {
//                             if (customer.validatePassword((req.body.password)))
//                                 return done(null, customer);
//                             else
//                                 return done("Incorrect password");
//                         } else {
//                             return done("Customer doesn't exist");
//                         }
//                     }).catch(err => {
//                         return done(err);
//                     });
//             }
//         });
// }));