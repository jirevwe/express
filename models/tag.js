const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Tag = new Schema({
  name: {
    type: Schema.Types.String,
    required: true,
    unique: true,
    dropDups: true,
  },
  modified_at: { type: Schema.Types.Date, default: Date.now() },
  created_at: { type: Schema.Types.Date, default: Date.now() },
});

module.exports = mongoose.model('Tag', Tag);
