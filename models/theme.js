const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Theme = new Schema({
  name: { type: Schema.Types.String, required: true },
  description: { type: Schema.Types.String, required: true },
  feature_image: { type: Schema.Types.String },
  month: { type: Schema.Types.String, default: '2017-01', required: true },
  modified_at: { type: Schema.Types.Date, default: Date.now() },
  created_at: { type: Schema.Types.Date, default: Date.now() },
});

const slug = require('mongoose-url-slugs');

Theme.plugin(slug('name'));
module.exports = mongoose.model('Theme', Theme);
