const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');

const User = new Schema({
  username: {
    type: Schema.Types.String,
    required: true,
    dropDups: true,
    unique: true,
  },
  password: {
    type: Schema.Types.String,
    required: true,
  },
  security_token: {
    type: Schema.Types.String,
    required: true,
  },
  created_at: {
    type: Schema.Types.Date,
    default: Date.now,
  },
  modified_at: {
    type: Schema.Types.Date,
    default: Date.now,
  },
});

User.methods.encrypt = function encrypt(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(5), null);
};

User.methods.encryptToken = function encryptToken(securityToken) {
  return bcrypt.hashSync(securityToken, bcrypt.genSaltSync(5), null);
};

User.methods.validatePassword = function validatePassword(password) {
  return bcrypt.compareSync(password, this.password);
};

User.methods.validateToken = function validateToken(token) {
  return bcrypt.compareSync(token, this.security_token);
};

module.exports = mongoose.model('User', User);
