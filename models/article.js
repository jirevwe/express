const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Article = new Schema({
  title: {
    type: Schema.Types.String, required: true, unique: true, dropDups: true,
  },
  text: { type: Schema.Types.String, required: true },
  theme: { type: Schema.Types.ObjectId, ref: 'Theme' },
  feature_image: { type: Schema.Types.String },
  gallery_images: [{ type: Schema.Types.String }],
  is_featured: { type: Schema.Types.Boolean, default: false, required: true },
  author: { type: Schema.Types.String },
  tags: [{ type: Schema.Types.ObjectId, ref: 'Tag' }],
  created_at: { type: Schema.Types.Date, default: Date.now() },
  modified_at: { type: Schema.Types.Date, default: Date.now() },
});

const slug = require('mongoose-url-slugs');

Article.plugin(slug('title'));
module.exports = mongoose.model('Article', Article);
