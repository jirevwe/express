const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const validator = require('express-validator');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const flash = require('connect-flash');
const passport = require('passport');
const bluebird = require('bluebird');

const app = express();
require('dotenv').config();
require('./config/passport');

mongoose.connect(process.env.DB_URL, {
  keepAlive: true,
  promiseLibrary: bluebird,
});

// view engine setup
app.use(logger('dev'));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(cookieParser());
app.use(validator());
app.use(session({
  secret: 'jokong',
  resave: false,
  saveUninitialized: false,
  store: new MongoStore({ mongooseConnection: mongoose.connection }),
  cookie: { maxAge: 1800 * 600 * 1000 },
  secure: true,
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'node_modules')));
app.use('/docs', express.static(path.join(__dirname, 'out')));
app.use(require('compression')());

app.use((req, res, next) => {
  res.locals.user = req.user;
  res.locals.login = req.isAuthenticated();
  res.locals.session = req.session;
  res.locals.SERVER_URL = process.env.SERVER_URL;
  res.locals.PORT = process.env.PORT;
  next();
});

app.use('/api/theme/', require('./api/theme'));
app.use('/api/admin/', require('./api/user'));
app.use('/api/tag/', require('./api/tag'));

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
