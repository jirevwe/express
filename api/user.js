const express = require('express');

const router = express.Router();
const cors = require('cors');
const passport = require('passport');

router.use(cors());

/**
 * @method POST /api/admin/signup
 * @description Creates an Admin User
 * @param username admin username (should be at least 6 chars)
 * @param password admin password (should be at least 6 chars)
 */
router.post('/signup', (r, s, n) => {
  passport.authenticate('admin.signup', (err, user) => {
    if (err) {
      return s.status(401).json({ status: 'failed', err });
    }
    if (!user) {
      return s.status(401).json({ status: 'failed', data: 'authentication failed' });
    }
    r.login(user, (loginErr) => {
      if (loginErr) {
        s.json({ status: 'failed', err: loginErr });
      }
      return s.status(200).json({ status: 'success', data: user });
    });
    return 0;
  })(r, s, n);
});

/**
 * @method POST /api/admin/signin
 * @description Authenticates an Admin User
 * @param username admin username (should be at least 6 chars)
 * @param password admin password (should be at least 6 chars)
 */
router.post('/signin', (r, s, n) => {
  passport.authenticate('admin.signin', (err, user) => {
    if (err) {
      return s.status(401).json({ status: 'failed', err });
    }
    if (!user) {
      return s.status(401).json({ status: 'failed', data: 'authentication failed' });
    }
    r.login(user, (loginErr) => {
      if (loginErr) {
        s.json({ status: 'failed', err: loginErr });
      }
      return s.status(200).json({ status: 'success', data: user.security_token });
    });
    return 0;
  })(r, s, n);
});

module.exports = router;
