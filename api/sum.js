const User = require('../models/user');

const sum = (a, b) => a + b;

const forEach = function forEach(items, callback) {
  for (let index = 0; index < items.length; index++) {
    callback(items[index]);
  }
};

const user = User({
  username: 'raymond',
  created_at: Date.now,
  modified_at: Date.now,
});
user.password = user.encrypt('00000tukpe');
user.security_token = user.encryptToken('01234');

// console.log(user);

module.exports = { sum, forEach, user };
