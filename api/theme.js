const express = require('express');

const router = express.Router();
const utils = require('../utils/utils');
const Theme = require('../models/theme');
const Article = require('../models/article');
const cors = require('cors');

router.use(cors());

/**
 * @method POST /api/theme/all
 * @description gets all the themes
 * @param {Number} page the page to retrieve
 * @param {Number} count the number of itmes per page
 */
router.post('/all', (r, s) => {
  if (r.body.page == null || r.body.count == null) { return s.status(404).json({ status: 'failed', err: 'page and count not specified' }); }

  const page = r.body.page - 1;
  const { count } = r.body.count;

  if (page < 0 && count < 0) {
    Theme.find()
      .then(themes => s.status(200).json({ status: 'success', data: themes })).catch(err => s.status(404).json({ status: 'failed', err }));
  } else {
    Theme.find().skip(page * count).limit(count).sort({ month: -1 })
      .then((themes) => {
        Theme.count().then(themeCount => s.status(200).json({ status: 'success', data: themes, meta: { totalCount: themeCount, totalPages: Math.ceil(themeCount / count) } }));
      })
      .catch(err => s.status(404).json({ status: 'failed', err }));
  }
});

/**
 * @method POST /api/theme/one
 * @description gets one theme
 * @param id the theme id
 */
router.post('/one', (r, s) => {
  Theme.findById(r.body.id)
    .then((theme) => {
      if (theme) {
        Article.find({ theme: theme._id })
          .populate('tags', 'name')
          .populate('theme', 'name')
          .then((articles) => {
            if (articles) {
              return s.status(200).json({
                status: 'success',
                data: {
                  name: theme.name,
                  month: theme.month,
                  description: theme.description,
                  articles,
                },
              });
            }
            return s.status(404).json({ status: 'failed', err: 'no a valid article id' });
          })
          .catch(err => s.status(404).json({ status: 'failed', err }));
      } else { return s.status(404).json({ status: 'failed', err: 'No data for theme with month ('.concat(r.body.month).concat(')') }); }
    }).catch(err => s.status(404).json({ status: 'failed', err }));
});

/**
 * @method POST /api/theme/update
 * @description Updates the theme profile
 * @param {String} id the theme's id
 * @param {Object} update the update info
 * @example
 * {
    "_id" : "596f3cac5cf87fff6b3d2dc2",
    "update": {
        "name": "Street Tools",
        "description": "more text",
        "momth": "02/2019"
    }
 }
 */
router.post('/update', utils.adminAuth, (r, s) => {
  if (r.body.id == null || r.body.update == null) { return s.status(404).json({ status: 'failed', err: 'the theme id (id) was not supplied' }); }

  Theme.findByIdAndUpdate(r.body.id, { $set: r.body.update }, { new: true, upsert: true })
    .then((theme) => {
      theme.modified_at = Date.now();
      theme.save().then(() => s.status(200).json({ status: 'success', data: theme }));
    }).catch(err => s.status(404).json({ status: 'failed', err }));
});

/**
 * @method POST /api/theme/create
 * @description Creates the theme
 * @param {String} name the theme's name
 * @param {String} description the theme's description
 * @param {String} month the theme month
 * @example
 * {
        "name": "Street Tools",
        "description": "more text",
        "momth": "02/2019"
    }
 */
router.post('/create', utils.adminAuth, (r, s) => {
  if (r.body.name == null || r.body.description == null || r.body.month == null) { return s.status(404).json({ status: 'failed', err: 'some required parameters were not supplied' }); }

  Theme.findOneAndUpdate({ name: r.body.name }, { $set: r.body }, { new: true, upsert: true })
    .then(theme => s.status(200).json({ status: 'success', data: theme }))
    .catch(err => s.status(404).json({ status: 'failed', err }));
});

/**
 * @method POST /api/theme/delete
 * @description Deletes the theme profile
 * @param {String} id the theme's id
 * @example
 * {
    "_id" : "596f3cac5cf87fff6b3d2dc2",
 }
 */
router.post('/delete', utils.adminAuth, (r, s) => {
  if (r.body.id == null) { return s.status(404).json({ status: 'failed', err: 'the theme id (id) was not supplied' }); }
  Theme.findById(r.body.id)
    .then((theme) => {
      if (theme) {
        if (theme.articles.length === 0) {
          Theme.findByIdAndRemove(r.body.id).then(() => s.status(200).json({ status: 'success', data: 'Theme deleted' }));
        } else { return s.status(404).json({ status: 'failed', err: 'This theme contain articles that should be deleted first' }); }
      } else { return s.status(404).json({ status: 'failed', err: 'no a valid theme id' }); }
    })
    .catch(err => s.status(404).json({ status: 'failed', err }));
});

/**
 * @method POST /api/theme/update/slugs
 * @description regenerates slugs
 */
router.post('/update/slugs', (r, s) => {
  Theme.find().then((theme) => {
    theme.forEach((element) => {
      element.markModified('name');
      element.save();
    });
  }).then(() => s.status(200).json({ status: 'success', data: 'done' }))
    .catch(err => s.status(404).json({ status: 'failed', err }));
});

/**
 * @method POST /api/theme/update/dates
 * @description updates dates
 */
router.post('/update/dates', (r, s) => {
  Theme.find().then((themes) => {
    themes.forEach((element) => {
      element.month = '2017-01';
      element.save();
    });
  }).then(() => s.status(200).json({ status: 'success', data: 'done' })).catch(err => s.status(404).json({ status: 'failed', err }));
});

module.exports = router;
