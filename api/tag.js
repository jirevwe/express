let express = require('express');
let router = express.Router();
let utils = require('../utils/utils');
let Article = require('../models/article');
let Tag = require('../models/tag');
let cors = require("cors");
router.use(cors());

/**
 * @method POST /api/article/tag/create
 * @param {String} name tag name
 * {
  "name":"October"
}
 */
router.post('/create', utils.adminAuth, (r, s, n) => {
    Tag.findOneAndUpdate({ name: r.body.name }, { $set: { name: r.body.name } }, { new: true, upsert: true })
        .then(tag => {
            return s.status(200).json({ status: "success", data: tag });
        }).catch(err => {
            return s.status(404).json({ status: "failed", err: err });
        });
});

/**
 * @method POST /api/article/tag/all
 * @param {String} name tag name
 * @example
 * {
    "name":"October"
}
 */
router.post('/all', (r, s, n) => {
    Tag.find()
        .then(tag => {
            return s.status(200).json({ status: "success", data: tag });
        }).catch(err => {
            return s.status(404).json({ status: "failed", err: err });
        });
});

/**
 * @method POST /api/article/tag/delete
 * @param {String} name tag name
 * @example
 * {
  "name":"October"
}
 */
router.post('/delete', utils.adminAuth, (r, s, n) => {
    Tag.findOneAndRemove({ name: r.body.name })
        .then(() => {
            return s.status(200).json({ status: "success", data: "deleted tag" });
        }).catch(err => {
            return s.status(404).json({ status: "failed", err: err });
        });
});

module.exports = router;