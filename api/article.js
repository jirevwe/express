let express = require('express');
let router = express.Router();
let utils = require('../utils/utils');
let Article = require('../models/article');
let Tag = require('../models/tag');
let Theme = require('../models/theme');
let cors = require("cors");
router.use(cors());

/**
 * @method POST /api/article/all
 * @description gets all the articles
 * @param {String} id the article id
 * @param {Object} tags tags
 * @param {Number} page the page to retrieve
 * @param {Number} count the number of itmes per page
 * @example
 * {
    "page": 2,
    "count": 2,
    "tags": [
        "5a266ab04bb0abd6a7052e3a",
        "5a266ac64bb0abd6a7052ec8",
        "5a266ac14bb0abd6a7052ea8"
  ]
}
 */
router.post('/all', (r, s, n) => {
    if (r.body.page == null || r.body.count == null)
        return s.status(404).json({ status: "failed", err: "page and count not specified" });

    const page = r.body.page - 1;
    const count = r.body.count;

    if (r.body.tags) {
        Article.find({ tags: { $in: r.body.tags } }).skip(page * count).limit(count).sort({ created_at: -1 })
            .populate('tags', 'name')
            .populate('theme', 'name')
            .then(articles => {
                Article.count({ tags: { $in: r.body.tags } }).then(articleCount => {
                    return s.status(200).json({ status: "success", data: articles, meta: { totalCount: articleCount, totalPages: Math.ceil(articleCount / count) } });
                });
            }).catch(err => {
                return s.status(404).json({ status: "failed", err: err });
            });
    } else {
        Article.find().skip(page * count).limit(count)
            .populate('tags', 'name')
            .populate('theme', 'name')
            .then(articles => {
                Article.count().then(articleCount => {
                    return s.status(200).json({ status: "success", data: articles, meta: { totalCount: articleCount, totalPages: Math.ceil(articleCount / count) } });
                });
            }).catch(err => {
                return s.status(404).json({ status: "failed", err: err });
            });
    }
});

/**
 * @method POST /api/article/one
 * @description gets one article
 * @param id the article id
 */
router.post('/one', (r, s, n) => {
    Article.findById(r.body.id)
        .populate('tags', 'name')
        .populate('theme', 'name')
        .then(article => {
            if (article)
                return s.status(200).json({ status: "success", data: article });
            else
                return s.status(404).json({ status: "failed", err: "no a valid article id" });
        }).catch(err => {
            return s.status(404).json({ status: "failed", err: err });
        });
});

/**
 * @method POST /api/article/search
 * @param query query
 * @description find an article
 */
router.post('/search', (r, s, n) => {
    if (r.body.page == null || r.body.count == null)
        return s.status(404).json({ status: "failed", err: "page and count not specified" });

    const page = r.body.page - 1;
    const count = r.body.count;

    if (page < 0 && count < 0) {
        Article.find({ "title": { $regex: r.body.query, $options: 'i' } })
            .populate('tags', 'name')
            .populate('theme', 'name')
            .then(articles => {
                return s.status(200).json({
                    status: "success",
                    data: articles
                });
            }).catch(err => {
                return s.status(404).json({ status: "failed", err: err });
            });
    } else {
        Article.find({ "title": { $regex: r.body.query, $options: 'i' } }).skip(page * count).limit(count)
            .populate('tags', 'name')
            .populate('theme', 'name')
            .then(articles => {
                Article.count({ "title": { $regex: r.body.query, $options: 'i' } }).then(articleCount => {
                    return s.status(200).json({ status: "success", data: articles, meta: { totalCount: articleCount, totalPages: Math.ceil(articleCount / count) } });
                });
            }).catch(err => {
                return s.status(404).json({ status: "failed", err: err });
            });
    }
});

/**
 * @method POST /api/aritcle/featured
 * @param month {String} theme month
 */
router.post('/featured', (r, s, n) => {
    Theme.findOne({ month: r.body.month })
        .then(theme => {
            if (theme) {
                Article.find({ theme: theme._id })
                    .populate('tags', 'name')
                    .populate('theme', 'name')
                    .then(articles => {
                        if (articles)
                            return s.status(200).json({
                                status: "success", data: {
                                    name: theme.name,
                                    month: theme.month,
                                    description: theme.description,
                                    articles: articles
                                }
                            });
                        else
                            return s.status(404).json({ status: "failed", err: "no a valid article id" });
                    }).catch(err => {
                        return s.status(404).json({ status: "failed", err: err });
                    });
            }
            else
                return s.status(404).json({ status: 'failed', err: "No data for theme with month (".concat(r.body.month).concat(')') });
        }).catch(err => {
            return s.status(404).json({ status: 'failed', err: err });
        });
});

/**
 * @method POST /api/article/create
 * @description create a new article
 * @param {String} theme_id the theme id
 * @param {String} title the article name
 * @param {String} text the article text
 * @param {Object} tags a list of tag ids
 * @example
 * {
  "theme_id": "5a2666a54bb0abd6a70516e7",
  "title": "DJ flock",
  "text": "Flummoxed by job, kvetching W. zaps Iraq. Cozy sphinx waves quart jug of bad milk. A very bad quack might jinx zippy fowls. Few quips galvanized the mock jury box. Quick brown dogs jump over the lazy fox. The jay, pig, fox, zebra, and my wolves quack! Blowzy red vixens fight for a quick jump. Joaquin Phoenix was gazed by MTV for luck. A wizard’s job is to vex chumps quickly in fog. Watch \"Jeopardy!\", Alex Trebek's fun TV quiz game. Woven silk pyjamas exchanged for blue quartz. Brawny gods just",
  "tags": [
  	"5a266ab04bb0abd6a7052e3a",
    "5a266ac64bb0abd6a7052ec8",
    "5a266ac14bb0abd6a7052ea8"
  ]
}
 */
router.post('/create', utils.adminAuth, (r, s, n) => {
    if (r.body.theme_id == null || r.body.title == null || r.body.text == null || r.body.tags == null)
        return s.status(404).json({ status: "failed", err: "some required parameters were not supplied" });

    Theme.findOne({ _id: r.body.theme_id })
        .then(theme => {
            if (theme == null)
                return s.status(404).json({ status: "failed", err: "Theme doesn't exist" });

            Article.findOneAndUpdate({ title: r.body.title }, { $set: r.body }, { new: true, upsert: true })
                .then(article => {
                    return s.status(200).json({ status: "success", data: article });
                }).catch(err => {
                    return s.status(404).json({ status: "failed", err: "Article couldn't be created" });
                });
        })
        .catch(() => {
            return s.status(404).json({ status: "failed", err: "Theme doesn't exist" });
        });
});

/**
 * @method POST /api/article/delete
 * @description deletes and article
 * @param {String} id the aritcle id
 * {
  "id":"5a2a88f34bb0abd6a71b054b"
}
 */
router.post('/delete', utils.adminAuth, (r, s, n) => {
    Article.findById(r.body.id)
        .then(article => {
            if (article == null) {
                return s.status(404).json({ status: "failed", err: "Article doesn't exist" });
            }
        })
        .then(() => {
            Article.findByIdAndRemove(r.body.id)
                .then(() => {
                    return s.status(200).json({ status: "success", data: "Article Deleted" });
                }).catch(err => {
                    return s.status(404).json({ status: "failed", err: "Article couldn't be deleted" });
                });
        })
        .catch(err => {
            return s.status(404).json({ status: "failed", err: "Article couldn't be created" });
        });
});

/**
 * @method POST /api/aritcle/update
 * @description updates an article
 * @param id article id
 * @param update the fields to be updated
 */
router.post('/update', utils.adminAuth, (r, s, n) => {
    if (r.body.id == null || r.body.update == null)
        return s.status(404).json({ status: "failed", err: "some required parameters were not supplied" });

    delete r.body.update.slug;
    if (r.body.update.theme && r.body.update.theme._id)
        r.body.update.theme = r.body.update.theme._id

    Article.findByIdAndUpdate(r.body.id, { $set: r.body.update }, { new: true })
        .then(article => {
            return s.status(200).json({ status: "success", data: article });
        }).catch(err => {
            return s.status(404).json({ status: "failed", err: err });
        })
});

/**
 * @method POST /api/aritcle/gallery/add
 * @description adds an image from the article gallery
 * @param id article id
 * @param url the image url
 */
router.post('/gallery/add', utils.adminAuth, (r, s, n) => {
    if (r.body.id == null || r.body.url == null)
        return s.status(404).json({ status: "failed", err: "some required parameters were not supplied" });

    Article.findById(r.body.id)
        .then(article => {
            if (article.gallery_images.indexOf(r.body.url) === -1) {
                article.gallery_images.push(r.body.url);
                article.save().then(() => {
                    return s.status(200).json({ status: "success", data: article });
                });
            }
            else {
                return s.status(404).json({ status: "failed", err: "Image has been added already" });
            }
        }).catch(err => {
            return s.status(404).json({ status: "failed", err: err });
        })
});

/**
 * @method POST /api/aritcle/gallery/remove
 * @description removes an image from the article gallery
 * @param id article id
 * @param url the image url
 */
router.post('/gallery/remove', utils.adminAuth, (r, s, n) => {
    if (r.body.id == null || r.body.url == null)
        return s.status(404).json({ status: "failed", err: "some required parameters were not supplied" });

    Article.findByIdAndUpdate(r.body.id, { $pull: { gallery_images: r.body.url } }, { new: true })
        .then(article => {
            return s.status(200).json({ status: "success", data: article });
        }).catch(err => {
            return s.status(404).json({ status: "failed", err: err });
        })
});

/**
 * @method POST /api/article/update/slugs
 * @description regenerates slugs
 */
router.post('/update/slugs', (r, s, n) => {
    Article.find().then(articles => {
        articles.forEach((element) => {
            element.markModified('title')
            element.save();
        });
    }).then(() => {
        return s.status(200).json({ status: "success", data: "done" });
    }).catch(err => {
        return s.status(404).json({ status: "failed", err: err });
    });
});

module.exports = router;