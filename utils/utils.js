const User = require('../models/user');

exports.contains = function contains(array, attr, value) {
  for (let i = 0; i < array.length; i += 1) {
    if (array[i][attr].toString() === value.toString()) {
      return i;
    }
  }
  return -1;
};

exports.compareArray = function compareArray(array1, array2) {
  for (let i = 0; i < array2.length; i += 1) {
    if (array1.indexOf(array2[i]) !== -1) {
      return i;
    }
  }
  return -1;
};

exports.validateEmail = function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

exports.hasOwnProperty = function hasOwnProperty(obj, prop) {
  const proto = Object.getPrototypeOf(obj) || obj.constructor.prototype;
  return (prop in obj) &&
    (!(prop in proto) || proto[prop] !== obj[prop]);
};

exports.adminAuth = function adminAuth(r, s, n) {
  User.findOne({ security_token: r.headers.token })
    .then((user) => {
      if (user == null) {
        return s.json({
          status: 'failed',
          data: '(ADM 0x1) Authentication Error: The token is either not present or is incorrect',
        });
      }
      return n();
    });
};
